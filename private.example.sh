export C_PVPGN_BRANCH=develop  # or 'master'
export C_REALMNAME="ShortNameIGuess"
export C_IIP=192.168.1.1
export C_DESCRIPTION="Amazing, what else could it be"
export C_LOCATION="EU/Somewhere"
export C_CONTACT="me myself"
export C_EMAIL="example@example.org"
# in seconds how long to give players to quit when stopping the server
# pvpgn default is ~360, but for testing lets keep less
export C_DOWNDELAY=3
# uncomment if intend to use mariadb (prev. mysql), pre-create the db and user first
#export C_STORAGEPATH="sql:mode=mysql;host=127.0.0.1;name=DBNAME;user=DBUSER;pass=DBPASS;default=0"

