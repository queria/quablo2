#!/bin/bash
set -ex
WORKSPACE="$(dirname "$(readlink -f "$0")")"

if [[ "$*" =~ clean ]]; then
	rm -rf pvpgn-server-master pvpgn-server-devel *.tar.gz *.tar.xz
	shift
fi

if [[ -f private.sh ]]; then
	source private.sh
else
	source private.example.sh
fi

ACTIONS="${*:-pvpgn config}"

if [[ "$ACTIONS" =~ pvpgn ]]; then bash $WORKSPACE/setup.01.pvpgn.sh; fi
if [[ "$ACTIONS" =~ d2gs ]]; then bash $WORKSPACE/setup.02.d2gs.sh; fi
if [[ "$ACTIONS" =~ config ]]; then bash $WORKSPACE/setup.10.config.sh; fi

