#
# do not use, just reference
exit 0

DBNAME=d2
DBUSER=d2
DBPASS=d2pass
cat <<<"
CREATE DATABASE d2;
CREATE USER 'd2'@'%' identified by 'DBPASS';
GRANT ALL ON d2.* to 'd2'@'%';
" | sudo mysql

