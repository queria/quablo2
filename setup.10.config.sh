#!/bin/bash
WORKSPACE="${WORKSPACE:-$(dirname "$(readlink -f "$0")")}"

cd $WORKSPACE
set -ex

cd "$WORKSPACE/pvpgn/etc/pvpgn/"

setval() {
	local file="$1"
	local key="$2"
	local val="$3"
	if grep -q "^${key}\s" "$file"; then
		# change existing key
		sed -r "s/^($key\s+=\s+).*$/\1$val/" -i "$file"
	else
		# uncomment
		sed -r "0,/$key =/s/^(#\s*)?($key\s+=\s+).*$/\2$val/" -i "$file"
	fi
}

# for variables see private.example.sh

setval bnetd.conf allowed_clients d2dv,d2xp
setval bnetd.conf passfail_count 5
setval bnetd.conf max_concurrent_logins 6
setval bnetd.conf use_keepalive true
setval bnetd.conf max_conns_per_IP 12
setval bnetd.conf track 0
setval bnetd.conf location "\"${C_LOCATION}\""
setval bnetd.conf description "\"${C_DESCRIPTION}\""
setval bnetd.conf contact_name "\"${C_CONTACT}\""
setval bnetd.conf contact_email "\"${C_EMAIL}\""
setval bnetd.conf servername "\"${C_REALMNAME}\""
setval bnetd.conf servaddrs ${C_IIP}:6112
setval bnetd.conf shutdown_delay  ${C_DOWNDELAY}
if [[ ! -z "$C_STORAGEPATH" ]]; then
	setval bnetd.conf storage_path = "\"${C_STORAGEPATH}\""
fi


setval d2cs.conf realmname "\"${C_REALMNAME}\""
setval d2cs.conf servaddrs "${C_IIP}:6113"
setval d2cs.conf gameservlist "${C_IIP}"
setval d2cs.conf bnetdaddr "${C_IIP}:6112"
setval d2cs.conf allow_convert 1
setval d2cs.conf shutdown_delay "${C_DOWNDELAY}"

setval d2dbs.conf servaddrs "${C_IIP}:6114"
setval d2dbs.conf gameservlist "${C_IIP}"
setval d2dbs.conf shutdown_delay "${C_DOWNDELAY}"

REALMCONF=$(grep '^#\|^$' realm.conf; echo "\"${C_REALMNAME}\"  \"${C_REALMNAME}\"  ${C_IIP}:6113")
cat > realm.conf <<<"$REALMCONF"
