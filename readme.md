Qu's collection for boostrapping private D2 server

Heavily based on htorbov's script from https://forums.pvpgn.pro/viewtopic.php?id=1875

- first install prerequisites for compilation (see https://pvpgn.pro/pvpgn_installation.html),
-- i may add these here too, but not bothered atm much since i usually have all such devel things already
- prepare private.sh file (copy private.example.sh and modify)
- (optional) review/tweak settings in setup.10.config.sh
- run `./setup.sh`
- (optional) run `./setup.sh <pvpgn|d2gs|config>` for re-running just specific part (feel free to delete its artifacts)
