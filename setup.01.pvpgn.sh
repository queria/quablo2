#!/bin/bash
WORKSPACE="${WORKSPACE:-$(dirname "$(readlink -f "$0")")}"

cd $WORKSPACE
set -ex

branch=$C_PVPGN_BRANCH
if [[ ! -d pvpgn-server-$branch ]]; then
	curl -C - -O -L https://github.com/pvpgn/pvpgn-server/archive/$branch.tar.gz
	tar xf $branch.tar.gz
fi
cd pvpgn-server-$branch
[[ -d build ]] || mkdir build
cd build
cmake -D WITH_MYSQL=true -D CMAKE_INSTALL_PREFIX=$(readlink -f "../../pvpgn") ../
make -j$(lscpu -pCPU|tail -n1)
make install

#cp -vf $WORKSPACE/etc/* $WORKSPACE/pvpgn/etc/pvpgn/
